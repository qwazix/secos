//constants
var noOfStripes = 10;
var stripesDuration = 300;
var backgroundWidth = 2020;
var aspect = $(window).width()/backgroundWidth;
var stripeHeight = 100;
var once = true;

var usedStripes = Array();
var stripesUsed = 0;
var loadStripesInterval;

var car;
var slider;

var tframes = 91;

var show360timeout;
var hideBackgroundTimeout;
var mql = window.matchMedia("(max-device-width:450px), (max-width:450px)");

function reloadStripesOnRotate(mediaQueryList){
        if (!mediaQueryList.matches) startLoadingStripes(50);
    }

$(document).ready(function(){
    sizes()
//    $("body").css("overflow-y","scroll");
//    var bwidth = $("body").width()
//    $("body").css("overflow-y","");
//    var aspect = bwidth/backgroundWidth;
    
    mql.addListener(reloadStripesOnRotate);
    
    if ($('body').css("background-attachment")!="fixed") if (location.hash =="") startLoadingStripes(stripesDuration);
    $('.current .stripe').each(function(){
//        $(this).width(bwidth)
        $(this).height($(this).width()/backgroundWidth*stripeHeight);
    })
    
    
    if (supportsSvg()){
//        $('#svg3299').css('display','block');
//        $('.wrapper.header').css('background','transparent');
        $("path").css({opacity: 0})
        $("#path10462").animate({opacity:1},3000);
        alreadyShown = Array();
        var i=0
        while (alreadyShown.length<=242){
            var rand = randomFromTo(0,242);
            var del = 3000+i*10/(Math.sqrt(Math.sqrt(i)/1.8));
            if ($.inArray(rand, alreadyShown)==-1) {
                $('#g13163 path:nth-child('+rand+')').delay(del).animate({opacity:1},3000);
                alreadyShown.push(rand);
            }
            i++;
            if (alreadyShown.length==242) setTimeout(function(){
//                if ($("#addonStyle").length){
//                    $("#addonStyle").remove();
//                    $(".topMenu, .footer, .text, .indexPhotos").css('opacity',0).animate({'opacity': 1}, 800);
//                }
            }, del*0.6)
                //$('.wrapper.header').css('background-image','');
        }
    } else {
        $(".logoFallBack").show();
    }
});

$(window).resize(sizes)


function sizes(){
    $('.stripe').css("height","");
    aspect = $(window).width()/backgroundWidth;
    $(".subtitle").css({top:aspect*700});
//    $('.noUiSlider').css("top", 1080*aspect - 20);
//    $('.storeWrap').css("top", 1080*aspect*0.67);
    $('body').css('font-size',$(window).width()/80);
    if ($('body').css("background-attachment")!="fixed") $('.credits').css("top", 1080*aspect*0.96);
    
}

function fadeStripeIn(element){
    if($(element).attr("src")!="empty.png"){
        $(element).animate({opacity: 1},{duration:1200, queue:false, complete: function(){
//                        $(this).css({"height":"auto", width: "100%"});
                if ($(element).attr("data-isLastStripe")=="true"){
                    afterStripeAnimationFinished();
                }
        }})
    }
}

function loadStripes(hash){
        if (typeof hash == "undefined") var hash = window.location.hash;
        hash = hash.replace("#","");
        if (hash=="thanks") hash="store"
        var i = Math.round(Math.random()*noOfStripes);
        if (usedStripes[i]==true) {
            loadStripes();
            return;
        }
        $('.current .stripe').eq(i).load(function(){fadeStripeIn(this);});
        $('.current .stripe').eq(i).attr("src", "stripes/"+hash+"stripe-"+ i+".jpg?");
//        $('.current .stripe').eq(i).css("height","");
        stripesUsed++;  usedStripes[i]=true;
        if (stripesUsed>noOfStripes) {
            clearInterval(loadStripesInterval);
            $('.current .stripe').eq(i).attr("data-isLastStripe", "true");
            endLoadingStripes();
        }
    }
    
function startLoadingStripes(speed){
    stripesUsed = 0;
    usedStripes = Array();
    loadStripesInterval = setInterval(loadStripes, speed);
    
    
    $(".background").css("display","block")
    
    if ($(".background").length>1) $(".background").not(":eq(0)").remove();
    $(".background").clone().addClass("current").appendTo("body");
    $(".background.current .stripe").css({opacity:""});
    $(".background.current .stripe").removeAttr("data-islaststripe")
}

function endLoadingStripes(){
    
}

function afterStripeAnimationFinished(){
    if ($(".background.current").length) {
        $(".background:not(.current)").remove();
        $(".background.current").removeClass("current");
    }
//    $('body').css("background-image","url('site-background-stripe.jpg')")
    if (window.location.hash.replace('#','') == "info"){

    }
    if (window.location.hash.replace('#','') == "history"){
//        $('.background').css("display","block");
    }
    $('#content').removeClass('fade');
}
    
function randomFromTo(from, to){
   return Math.floor(Math.random() * (to - from + 1) + from);
}

function supportsSvg() {
//    if ((jQuery.browser.opera && jQuery.browser.version < 12) ||(jQuery.browser.mozilla && jQuery.browser.version.substring(0,1) == 1)) return false;
    return !!document.createElementNS && !!document.createElementNS('http://www.w3.org/2000/svg', "svg").createSVGRect;
}


var origContent = "";

function loadContent(hash) {
    if (typeof historyRotation == "function") mql.removeListener(historyRotation);
    hash = hash.replace("#","");
    if(hash != "") {
        $('.mobmenu').css('opacity','0'); //krivo to menu mobile an itan emfanismeno
        $('.mobmenu').css('height','0px');
        $('.mobmenu').css('padding','0px');
        if(origContent == "") {
            origContent = $('#content').html();
        }
        $('#content').addClass('fade');
        show360timeout = setTimeout(function(){
            var url = location.hash.replace(/^.*#/, '');
            
            var bodyClass = url.replace(".html","");
            if (bodyClass == "") bodyClass= "index";
            $("body").attr("class","").addClass(bodyClass);
            
            $('#content').hide(0);
            $('#content').load(hash +".html", function(){ 
                $('#content').show(); 
                sizes();
                if ($('body').css("background-attachment")=="fixed") $('#content').removeClass('fade');
                                
                if (location.hash.replace("#",'')=="info"){
                     $('.more').click(function(){
                        if ($('.info .maintext').css("display")=="none")
                            $('.info .maintext').fadeIn();
                        else
                            $('.info .maintext').fadeOut();
                    })
                    var imgPath;
                    if ($('body').css("background-attachment")=="fixed"){
                        imgPath = "360mob/";
                    } else {
                        imgPath = "360/";
                    }

                    car = $('.car').ThreeSixty({
                        totalFrames: tframes, // Total no. of image you have for 360 slider
                        endFrame: tframes, // end frame for the auto spin animation
                        currentFrame: 1, // This the start frame for auto spin
                        imgList: '.threesixty_images', // selector for image list
                        progress: '.spinner', // selector to show the loading progress
                        imagePath: imgPath, // path of the image assets
                        filePrefix: '', // file prefix if any
                        ext: '.jpg', // extention for the assets
                        height: "auto",
                        width: "auto",
                        navigation: false,
                        disableSpin: true,
                        dontTrigger: true,
                        loaded: function(){
                            $('#nouislider').fadeIn();
//                            hideBackgroundTimeout = setTimeout(function(){
//                                if (location.hash.replace('#','') == "info"){
//                                    $('.background').fadeOut();
                                    
//                                }
//                                
//                            }, 100)
                        },
                        frameChanged: function(currentFrame){
                            var frame = (10000+currentFrame)%tframes;
                            if (once) { //the first time the whole thing jumps for some reason
                                once = false
                            } else {
                                $("#nouislider").val(frame)
                            }
                            console.log(frame)
                            
                        }
                    });

                    slider = $('#nouislider').noUiSlider({
                        handles: 1,
                        range: [1,tframes],
                        start: 1,
                        connect: "lower",
                        slide: function() {
    //                      var values = $(this).noUiSlider( 'value' )[1];
    //                      frame = Math.round(90 * values / 100);
                          car.gotoAndPlay(Math.round($(this).val()), false);
                        }
                      });

                }

            });
        }, 300);
    } else if(origContent != "") {
        $('#content').html(origContent);
    } 
}

$(document).ready(function() {
    //onHashChange callback    
    $('.mobmenu').css('opacity','0'); //krivo to menu mobile an itan emfanismeno
        $('.mobmenu').css('height','0px');
        $('.mobmenu').css('padding','0px');
   
    if (window.location.hash=='') {
        $('.menuhandler').attr('href','#')
    } else {
         $('.menuhandler').attr('href',window.location.hash) //xreiazetai gia na douleyei to menuhandler
    }
     
    window.onhashchange = function(){
        //reset a couple of things
        $('.background').css({
            display: "block",
            opacity: 1
        })
//        clearTimeout(hideBackgroundTimeout);
        clearTimeout(show360timeout);
        
        
        // do the actual work
        loadContent(location.hash)
        if ($('body').css("background-attachment")!="fixed") startLoadingStripes(50);
    }//telos onHashChange

    //if dom ready the hash isnt empty trigger the onhashChange
    if(window.location.hash!=""){
        window.onhashchange();
    }

    $('a[href^="#"]').click(function(e) {
//        clearTimeout(hideBackgroundTimeout)
        var url = $(this).attr('href').replace(/^.*#/, '');
        //alert(url)
        $('.menuhandler').attr('href','#'+url) //xreiazetai gia na douleyei to menuhandler
        window.location.hash=url;
        clearInterval(loadStripesInterval);
        return false;
    });//telos onClick
});//telos dom ready
  
function onformpost(){
        $('.error').removeClass('error');
        var originalSendBtnContainerHtml= $('.sendContainer').html();
        if ($('#contactform input[name=name]').val() == "name" || $('#contactform input[name=name]').val() =="") $('#contactform input[name=name]').addClass('error');
        if ($('#contactform input[name=email]').val() == "email" || $('#contactform input[name=email]').val() =="") $('#contactform input[name=email]').addClass('error');
        if ($('#contactform textarea').val() == "Type your message here" || $('#contactform textarea').val() =="") $('#contactform textarea').addClass('error');
//        if ($('.error').length==0) $('.sendContainer').html('<img src="images/ajax-loader.gif">');

        $.get('ajax.php?action=contactForm', $("#contactform").serialize() ,
            function(data){
                text=" ";
                result=" ";
                if(data=="email"){
                    text="Please write your email address";
                    $('.sendContainer').html(originalSendBtnContainerHtml);
                } else if(data=="notvalidemail"){
                    text="Something is wrong with this email address!";
                    $('.sendContainer').html(originalSendBtnContainerHtml);
                }else if(data=="name"){
                    text="Please write your name!";
                    $('.sendContainer').html(originalSendBtnContainerHtml);
                }else if(data=="message"){
                    text="Please leave a message!";
                    $('.sendContainer').html(originalSendBtnContainerHtml);
                }else if(data=="sent") {
                    result="Message sent!";
                    //$('.sendbtn').hide();
                    //$('#contactform .result').css('font-size','20px');
                    //$('#contactform .error').prev().children('span').html('');
                    $('#contactform .error').removeClass('error');
                    $('.sendContainer').html(result);
                    }
                else if(data=="error") {
                    result="An error occured. Please refresh the page and try again.";
                    $('.sendContainer').html(result);
                }
                
                $('*[name='+data+']').addClass('error');
                //$('*[name='+data+']').prev().children('span').html(text);

            }
        );
  }


function preload(arrayOfImages) {
    $(arrayOfImages).each(function(){
        $('<img/>')[0].src = this;
        // Alternatively you could use:
        // (new Image()).src = this;
    });
}
